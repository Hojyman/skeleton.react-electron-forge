module.exports = {
  extends: ['eslint-config-airbnb'],
  parser: 'babel-eslint',
  rules: {
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          "**/*.test.js",
          "**/*.spec.js",
          "src/main.js",
          "src/tools/*.js",
          "*.config.js",
        ],
        // "peerDependencies": true
      }
    ],
    // 'react/jsx-filename-extension': 0,
    // 'react/jsx-props-no-spreading': 0,
    // 'react/forbid-prop-types': 0,
    // 'import/prefer-default-export': 0,
  }
}