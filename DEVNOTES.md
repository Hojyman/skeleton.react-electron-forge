## [Setup] separate Content-Security-Policy for production

```sh
npm add -D html-replace-webpack-plugin
```

__webpack.renderer.config.js__
```js
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');

const isBuild = process.env.NODE_ENV !== 'development';

// ...
  plugins: [
    new HtmlReplaceWebpackPlugin([
      {
        pattern: '@@Content-Security-Policy',
        replacement: isBuild
          ? '<meta http-equiv="Content-Security-Policy" content="script-src \'self\';">'
          : '',
      },
    ]),
  ],

```

__src/index.html__
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    @@Content-Security-Policy
    <title>Hello World!</title>

  </head>
  <body>
    <div id="appRoot" />
  </body>
</html>
```


## [Setup] Blocking console in production mode

__src/maim.js__
```js
const getDevelopmentModeParams = () => {
  if (process.env.NODE_ENV !== 'development') {
    return ({
      devTools: false,
    });
  }
  return {};
};

// ...

  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      ...getDevelopmentModeParams(),
      nodeIntegration: true,
    },
  });
```


## [Setup] SCSS support setup

```sh
npm add -D style-loader css-loader postcss-loader sass-loader autoprefixer postcss-easy-import
npm add -D node-sass
```

__src/tools/scss-loader.js__
```js
const autoprefixer = require('autoprefixer');
const cssEasyImport = require('postcss-easy-import');

const scssLoaders = (isBuild = true, modules = false) => [
  {
    loader: 'style-loader',
    options: {
      sourceMap: !isBuild,
    },
  },
  {
    loader: 'css-loader',
    options: {
      importLoaders: 1,
      modules,
      // url: false,
      // localIdentName: '[name]_[local]_[hash:base64:5]',
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: !isBuild,
      plugins: () => [
        autoprefixer,
        cssEasyImport,
      ],
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: !isBuild,
    },
  },
];

module.exports = scssLoaders;
```

__webpack.renderer.config.js__
```js
const rules = require('./webpack.rules');
const scssLoaders = require('./src/tools/scss-loader');

const isBuild = process.env.NODE_ENV === 'production';

rules.push({
  test: /\.module\.(css|scss)$/,
  use: scssLoaders(isBuild, true),
});

rules.push({
  test: /\.(css|scss)$/,
  exclude: /\.module\.(css|scss)$/,
  use: scssLoaders(isBuild, false),
});
```


## [Setup] enabling development/production optimization

```sh
npm add -D cross-env
```

__package.json__
```
  "scripts": {
    "start": "cross-env NODE_ENV=development electron-forge start",
```

__src/tools/process-env-setup.js__
```js
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'production';
}
process.env.BABEL_ENV = process.env.NODE_ENV;
```

__webpack.rules.js__
```
require('./src/tools/process-env-setup'); // must be the first line of code
```

## [Setup] ESLint (airbnb) setup

```sh
npm add -D eslint babel-eslint 
npm add -D eslint-config-airbnb eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import
```

__.eslintrc.js__
```
module.exports = {
  extends: ['eslint-config-airbnb'],
  parser: 'babel-eslint',
  rules: {
    // 'import/no-extraneous-dependencies': [
    //   'error', {devDependencies: true}
    // ],
    // 'react/jsx-filename-extension': 0,
    // 'react/jsx-props-no-spreading': 0,
    // 'react/forbid-prop-types': 0,
    // 'import/prefer-default-export': 0,
  }
}
```

__package.json__
```
  "scripts": {
    ...
    "esfix": "eslint src --ext .js --ext .jsx --fix",
    "esfix:all": "eslint ./ --ext .js --ext .jsx --fix",
    ...
  },
```


## [Setup][Improve] Add class extented properties support

__resolve.config.js__
```js
const path = require('path');
const rootFolder = path.resolve('./')

module.exports = {
  root: [rootFolder],
  alias: {
    '@kard': path.join(rootFolder, './packages/@kard'),
  },
}
```

__.babelrc.js__
```js
module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-react',
  ],
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    [ 'babel-plugin-module-resolver', require('./resolve.config')],
  ],
};
```


## [Setup] Add class extented properties support

```sh
npm add -D @babel/plugin-proposal-class-properties
```

```js
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    //...
```


## [Setup] Add Babel Module Resolver

```sh
npm add -D babel-plugin-module-resolver
```

__.babelrc.js__
```js
  plugins: [
    [ 'babel-plugin-module-resolver',
      {
        root: [rootFolder],
        alias: {
          '@kard': path.join(rootFolder, './packages/@kard'),
        },
      },
    ],
  ],
```

## [Setup][Fix] Add hooks support to Webpack HMR

__webpack.rules.config.js__ (for hooks support)
```js
  {
      test: /\.js$/,
      include: /node_modules\/react-dom/,
      use: ['react-hot-loader/webpack']
  },
```


## [Setup] Enabling Node.js integration

To enable Node.js integration in this file, open up `main.js` and enable the `nodeIntegration` flag
```
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
  });
```

## [Setup][Fix] Add `.jsx` extension support

__webpack.renderer.config.js__
```js
  resolve: {
    // ...
    extensions: ['.js', '.jsx'],
  },
```


## [REMOVED][̶S̶e̶t̶u̶p̶] ̶A̶d̶d̶ ̶h̶o̶o̶k̶s̶ ̶s̶u̶p̶p̶o̶r̶t̶ ̶t̶o̶ ̶W̶e̶b̶p̶a̶c̶k̶ ̶H̶M̶R̶

```sh
npm add @hot-loader/react-dom # for hooks support
```

__webpack.renderer.config.js__ (for hooks support)
```js
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
```

## [Setup] Add Webpack Hot Modules Reload (HMR) support

```sh
npm add -D react-hot-loader
```

__renderer.js__
```js
import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader';

import App from './app';

ReactDOM.render(
  React.createElement(
    hot(module)(App), 
    null, 
    null,
  ),
  document.getElementById('appRoot'),
)
```

## [Setup][Fix] React. Dealing with `Electron Security Warning (Insecure Content-Security-Policy)`

__main.js__
```js
// at the first line
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
```

__src/index.html__
```html
  <head>
    ...
    <!-- <meta http-equiv="Content-Security-Policy" content="script-src 'self';"> -->
    ...
  </head>
```

refs: 
* [getting react hot reloading working](https://github.com/electron-userland/electron-forge/issues/1164#issuecomment-538434543)
* [Why do I see an “Electron Security Warning”?](https://stackoverflow.com/a/57553252)
* [Allow All Content Security Policy](https://stackoverflow.com/a/35997557)
* [Content Security Policy Reference](https://content-security-policy.com/)
* [How to prevent Electron Security Warning](https://github.com/electron/electron/issues/19775#issuecomment-522289694)
* [Electron, Security, Native Capabilities, and Your Responsibility](https://www.electronjs.org/docs/tutorial/security)


## [Setup] React. Dealing with `Electron Security Warning (Insecure Content-Security-Policy)`

__src/index.html__
```html
  <head>
    ...
    <meta http-equiv="Content-Security-Policy" content="script-src 'self';">
    ...
  </head>
```


## [Setup] add jsx support

```sh
npm add -D babel-loader@8.0.0-beta.0 @babel/core @babel/preset-env @babel/preset-react
```

__babelrc.js__
```js
module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-react',
  ],
};
```

__webpack.rules.js__
```js
  {
    test: /\.(js|jsx)$/,
    exclude: [/node_modules/],
    use: [{
      loader: 'babel-loader',
      options: require('./.babelrc'),
   }],
  },
```

__src/renderer.js__
```jsx
ReactDOM.render(
  <Hello toWhat='мир'>Привет</Hello>,
  document.getElementById('appRoot'),
  renderCB,
)
```


## [Setup] add react to the project

```sh
npm add react react-dom
```

__src/index.html__
```html
    <div id="appRoot" />
```

__src/renderer.js__
```js
import React from 'react';
import ReactDOM from 'react-dom';

const renderCB = () =>
  console.log('👋 This message is being logged by "renderer.js", included via webpack');

class Hello extends React.Component {
  render() {
    return React.createElement('div', null, `Привет, ${this.props.toWhat}`);
  }
}

ReactDOM.render(
  React.createElement(Hello, {toWhat: 'мир'}, null),
  document.getElementById('appRoot'),
  renderCB,
)
```