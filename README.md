# React + Electrone Forge skeleton


## How to use

```sh
git clone https://gitlab.com/Hojyman/skeleton.react-electron-forge.git my-project
cd $_
npm ci
npm start
```

References:
* [How to make it from scratch](DEVNOTES.md)
* [Electrone Forge](https://www.electronforge.io/)


## Features

* react
* babel
* webpack
* webpack-dev-server + HMR
* Sass/SCSS
* eslint + airbnb setup
* advanced code aliasing


## Scripts

`npm run package` - make application for the currently used platform

`npm start` - start developmant

`npm run esfix` - run eslint checking/fixing for all js/jsx files inside src folder

`npm run esfix:all` - run eslint checking/fixing for all js/jsx files (include webpack configs and so on)
