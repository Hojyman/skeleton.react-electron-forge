/* NOTE: consider using __dirname to provide a path relative to your configuration */
const path = require('path');

const rootFolder = path.resolve('./');

module.exports = {
  root: [rootFolder],
  alias: {
    '@kard': path.join(rootFolder, './packages/@kard'),
  },
};
