import React from 'react';

/* eslint-disable jsx-a11y/accessible-emoji */
export default function Hello() {
  return (
    <div>
      <h1>💖 Hello World!</h1>
      <p>Welcome to your Electron application.</p>
    </div>
  );
}
