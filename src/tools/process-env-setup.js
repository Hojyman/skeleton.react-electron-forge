if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'production';
}
process.env.BABEL_ENV = process.env.NODE_ENV;
