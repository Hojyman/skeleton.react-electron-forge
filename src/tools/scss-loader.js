const autoprefixer = require('autoprefixer');
const cssEasyImport = require('postcss-easy-import');

const scssLoaders = (isBuild = true, modules = false) => [
  {
    loader: 'style-loader',
    options: {
      sourceMap: !isBuild,
    },
  },
  {
    loader: 'css-loader',
    options: {
      importLoaders: 1,
      modules,
      // url: false,
      // localIdentName: '[name]_[local]_[hash:base64:5]',
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: !isBuild,
      plugins: () => [
        autoprefixer,
        cssEasyImport,
      ],
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: !isBuild,
    },
  },
];

module.exports = scssLoaders;
