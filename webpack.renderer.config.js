const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');

const rules = require('./webpack.rules');
const scssLoaders = require('./src/tools/scss-loader');

const isBuild = process.env.NODE_ENV !== 'development';

rules.push({
  test: /\.module\.(css|scss)$/,
  use: scssLoaders(isBuild, true),
});

rules.push({
  test: /\.(css|scss)$/,
  exclude: /\.module\.(css|scss)$/,
  use: scssLoaders(isBuild, false),
});

module.exports = {
  // Put your normal webpack config below here
  module: {
    rules,
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new HtmlReplaceWebpackPlugin([
      {
        pattern: '@@Content-Security-Policy',
        replacement: isBuild
          ? '<meta http-equiv="Content-Security-Policy" content="script-src \'self\';">'
          : '',
      },
    ]),
  ],
};
