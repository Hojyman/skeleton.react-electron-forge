require('./src/tools/process-env-setup');
const babelConfig = require('./.babelrc');

module.exports = [
  // Add support for native node modules
  {
    test: /\.node$/,
    use: 'node-loader',
  },
  {
    test: /\.(m?js|node)$/,
    parser: { amd: false },
    use: {
      loader: '@marshallofsound/webpack-asset-relocator-loader',
      options: {
        outputAssetBase: 'native_modules',
      },
    },
  },
  {
    test: /\.(js|jsx)$/,
    exclude: [/node_modules/],
    use: [{
      loader: 'babel-loader',
      options: babelConfig, // babelOptions,

      /* NOTE: Parsing of .babelrs pattern */
      // options: {
      //   ...JSON.parse(fs.readFileSync(path.resolve(__dirname, './.babelrc'))),
      // }

    }],
  },
  {
    test: /\.js$/,
    include: /node_modules\/react-dom/,
    use: ['react-hot-loader/webpack'],
  },

  // Put your webpack loader rules in this array.  This is where you would put
  // your ts-loader configuration for instance:
  /**
   * Typescript Example:
   *
   * {
   *   test: /\.tsx?$/,
   *   exclude: /(node_modules|.webpack)/,
   *   loaders: [{
   *     loader: 'ts-loader',
   *     options: {
   *       transpileOnly: true
   *     }
   *   }]
   * }
   */
];
